# DPLL
My implementation of [DPLL algorithm](https://en.wikipedia.org/wiki/DPLL_algorithm).

## Features
- DPLL algorithm,
- parsing [CNF files](https://people.sc.fsu.edu/~jburkardt/data/cnf/cnf.html) (i.e. files containing representation of formula in CNF format),
- Sandwich problem solution.

## Tests
There are some basic tests (in `test/Spec.hs`) and one less trivial, i.e. solution to the *sandwich problem*.

### Sandwich problem
Let `n` be positive integer. Correct sandwich is a ordering of numbers
1, 2, ...,`n`, 1, 2, ..., `n` that holds that
for each pair of positions with same number `i` in the ordering
there are exactly `i` numbers in between.

#### Example: correct sandwich for `n = 4`
4, 1, 3, 1, 2, 4, 3, 2

In the repository there are example files for this problem for
- `n = 4`:
  - `4,1,3,1,2,4,3,2`
- `n = 7`:
  - `1,7,1,2,5,6,2,3,4,7,5,3,6,4`
- `n = 8`:
  - `1,7,1,2,8,6,2,3,5,7,4,3,6,8,5,4`

You can try them by setting `problemSize` constant in file `app/Main.hs`.
