module DPLL where
  import Data.List
  import Data.Map (Map)
  import qualified Data.Map as Map
  
  import Data.Maybe
  import Debug.Trace
  
  import CommonTypes
  
  debug = flip trace

  -- checks if literal is satisfied in the model
  literalSatisfied :: Variable -> Model -> Bool
  literalSatisfied lit m = ans
    where
      ans =
        Map.member (abs lit) m &&
          ((lit > 0 && m Map.! abs lit) || (lit < 0 && not (m Map.! abs lit)))

  -- check if clause is satisfied in the model
  clauseSatisfied :: Clause -> Model -> Bool
  clauseSatisfied c m = any (`literalSatisfied` m) c

  -- check if formula is satisfied in the model
  formulaSatisfied :: Formula -> Model -> Bool
  formulaSatisfied f m = all (`clauseSatisfied` m) f
  
  -- simplify formula by assigning value to the variable
  assignVariable :: (Variable, Bool) -> Formula -> Formula
  assignVariable (var, val) f = withoutLits
    where
      targetLiteral = if val then var else -var
      -- remove clauses that contains satisfied literal
      -- remove variables from clauses that are in incorrect literal
        -- if there is an empty clause it means that formula can't be satisfied
        -- i.e. return Right False (that will be checked in main algorithm)
      withoutClauses = filter (\clause -> targetLiteral `notElem` clause) f
      withoutLits = map (filter (/= -targetLiteral)) withoutClauses

  -- check if variable is pure (pure means that variable is always either positive or negative (not mixed))
  isPureSymbol :: Variable -> Formula -> Maybe Bool
  isPureSymbol var f = if noPositive /= noNegative then
                          if noNegative then Just True
                          else Just False
                        else Nothing
    where
      noPositive = null (filter (\clause -> var `elem` clause && (-var) `notElem` clause) f)
      noNegative = null (filter (\clause -> (-var) `elem` clause && var `notElem` clause) f)

  -- get the pure symbol & value to assign it with
  getPureSymbol :: Int -> Formula -> Maybe (Variable, Bool)
  getPureSymbol varCount f =
      if null pureSymbols then
        Nothing
      else
        Just $ head pureSymbols
    where
      pureSymbols = mapMaybe (\var ->
        if isJust (isPureSymbol var f) then
          Just (var, fromJust (isPureSymbol var f))
        else
          Nothing) [1..varCount]

  -- get unitary symbol (i.e. variable that is alone in some clause)
  getUnitarySymbol :: Formula -> Maybe (Variable, Bool)
  getUnitarySymbol f = if null unitaryClauses then Nothing else Just (abs (head (head unitaryClauses)), head (head unitaryClauses) > 0)
    where
      unitaryClauses = filter (\clause -> length clause == 1) f

  -- main algorithm
  dpll :: Int -> Formula -> Model -> Either Model Bool
  dpll varCount f m
    | [] `elem` f             = Right False   -- there is an empty clause (i.e. formula is no longer satisfiable)
    | notSat                  = Right False   -- there are no free variables & 'f' not satisfied by 'm'
    | formulaSatisfied f m    = Left m        -- model 'm' satisfies formula 'f'
    | otherwise               = bestGuess     -- there are free variables -> try both assignments
    where
      allFreeVariables = filter (\var -> not (Map.member var m)) [1..varCount]
      freeVariable = head allFreeVariables
      notSat = null allFreeVariables && f /= []
      unitarySymbol = getUnitarySymbol f
      pureSymbol = getPureSymbol varCount f

      trueGuess = dpll varCount (assignVariable (abs freeVariable, True) f) (Map.insert freeVariable True m)
      falseGuess = dpll varCount (assignVariable (abs freeVariable, False) f) (Map.insert freeVariable False m)

      bruteForceGuess =
        case [trueGuess, falseGuess] of
          [Left x, _]       -> Left x
          [Right _, Left x] -> Left x
          _                 -> Right False

      bestGuess
        | isJust unitarySymbol  = dpll varCount (assignVariable (fromJust unitarySymbol) f) (Map.insert (fst (fromJust unitarySymbol)) (snd (fromJust unitarySymbol)) m)-- `debug` ("UNITARY " ++ show unitarySymbol)
        | isJust pureSymbol     = dpll varCount (assignVariable (fromJust pureSymbol) f) (Map.insert (fst (fromJust pureSymbol)) (snd (fromJust pureSymbol)) m)-- `debug` ("PURE " ++ show pureSymbol)
        | otherwise             = bruteForceGuess-- `debug` ("BRUTE FORCE " ++ show freeVariable)
