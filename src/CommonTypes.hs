module CommonTypes where  
  import Data.Map (Map)
  import qualified Data.Map as Map

  type Variable = Int
  type Clause = [Variable]
  type Formula = [Clause]
  type Model = Map Variable Bool  -- e.g. [(1, True), (2, False), (3, False), ...]
