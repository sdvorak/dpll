module SandwichProblem where
  import Data.Map (Map)
  import qualified Data.Map as Map
  import Data.List (sortBy)
  import Data.Function (on)

  import CommonTypes

  chunks :: Int -> [a] -> [[a]]
  chunks _ [] = []
  chunks chunkSize l = (take chunkSize l):chunks chunkSize (drop chunkSize l)

  translateHelper :: Ord a => [[a]] -> [(a, Int)]
  translateHelper x = ans
    where
      ans1 = concatMap (\(val, [first, second]) -> [(first, val), (second, val)]) (zip [1..] x)
      ans = sortBy ((\(s1, _) (s2, _) -> compare s1 s2)) ans1

  sandwichTranslate :: Either Model Bool -> Int -> IO ()
  sandwichTranslate (Left m) solutionLength = do
    let x = chunks solutionLength (Map.toAscList m)
        y = map (\c -> map (\q -> if (fst q) `mod` solutionLength /= 0 then (fst q) `mod` solutionLength else solutionLength) (filter (\pair -> snd pair) c)) x
        z = translateHelper y
        z2 = map snd z
    print z2

  sandwichTranslate (Right _) _ = do
    print "No solution."

