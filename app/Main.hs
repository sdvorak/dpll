module Main where

import Data.List
import Data.Map (Map)
import qualified Data.Map as Map

import DPLL
import SandwichProblem

main :: IO ()
main = do
  ------------------------
  -- Set problem size
  let problemSize = 4
  ------------------------
  
  text <- readFile ("test/data/n=" ++ show problemSize ++ ".cnf")
  let allLines = lines text
      headers = filter (isPrefixOf "p") allLines
      header = head headers
      headerSplit = words header
      varCount = read (headerSplit !! 2) :: Int
      clauseCount = read (headerSplit !! 3) :: Int
      withoutComments = filter (\var -> not (isPrefixOf "c" var) && not (isPrefixOf "p" var)) allLines

      -- create formula
      parsedClauses = map (\clause -> let s = words clause in map (\x -> read x :: Int) s) withoutComments
      -- remove trailing zeroes
      withoutTrailingZeroes = map init parsedClauses
      -- get model
      model = dpll varCount withoutTrailingZeroes Map.empty

  if length headers /= 1 then
    error "There must be ONLY ONE header (line that starts with 'p')"
  else
    sandwichTranslate model (problemSize * 2)
