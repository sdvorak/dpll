import Data.Map (Map)
import qualified Data.Map as Map

import CommonTypes
import DPLL

testDPLL :: Bool
testDPLL = and testCases
  where
    testCases = [
      dpll 4 [[1,2,-3], [-2,3,4], [-1,-2,-3]] Map.empty == Left (Map.fromList [(3,False),(4,True)]),
      dpll 4 [[1], [2], [3], [4]] Map.empty == Left (Map.fromList [(1,True),(2,True),(3,True),(4,True)]),
      dpll 4 [[1], [2], [-3], [4]] Map.empty == Left (Map.fromList [(1,True),(2,True),(3,False),(4,True)]),
      dpll 4 [[-2], [2], [-3], [-1,-4]] Map.empty == Right False,
      dpll 3 [[1], [-1,3], [2]] Map.empty == Left (Map.fromList [(1,True),(2,True),(3,True)])]

-- 2-coloring of path P_3
testTwoColoring :: Bool
testTwoColoring = dpll 6 [[-1, -3], [-2, -4], [-3, -5], [-4, -6], [1, 2], [3, 4], [5, 6]] Map.empty == Left (Map.fromList [(1,True),(2,False),(3,False),(4,True),(5,True),(6,False)])


testLiteralSatisfied :: Bool
testLiteralSatisfied = and testCases
  where
    testCases = [
      literalSatisfied 1 (Map.fromList [(2, False), (3, True), (1, True)]) == True,
      literalSatisfied (-1) (Map.fromList [(2, False), (3, True), (1, True)]) == False
      ]

testClauseSatisfied :: Bool
testClauseSatisfied = and testCases
  where
    testCases = [
      clauseSatisfied [1,2,-3,4,-5] (Map.fromList [(2, False), (3, True), (1, True)]) == True,
      clauseSatisfied [1,2,-3,4,-5] (Map.fromList [(-2, False), (3, True), (1, True)]) == True,
      clauseSatisfied [1,2,-3,4,-5] (Map.fromList [(-2, False), (3, True), (-1, True)]) == False
      ]

testFormulaSatisfied :: Bool
testFormulaSatisfied = and testCases
  where
    testCases = [
      formulaSatisfied [[1,2,-3], [-2,3,4], [-1,-2,-3,4], [5]] (Map.fromList [(2, False), (3, True), (1, True), (5, True)]) == True,
      formulaSatisfied [[1,2,-3], [-2,3,4], [-1,-2,-3,4], [-5]] (Map.fromList [(2, False), (3, True), (1, True), (5, True)]) == False,
      formulaSatisfied [[1,2,-3], [-2,3,4], [-1,-2,-3,4], [5]] (Map.fromList [(2, False), (3, True), (1, True), (5, True)]) == True,
      formulaSatisfied [[1,2,-3], [-2,3,4], [-1,2,-3,4], [5]] (Map.fromList [(2, False), (3, True), (1, True), (5, True)]) == False
      ]



main :: IO ()
main = do
  let t1 = testDPLL
      t2 = testTwoColoring
      t3 = testLiteralSatisfied
      t4 = testClauseSatisfied
      t5 = testFormulaSatisfied

  print ("Tests went: " ++ (if and [t1, t2, t3, t4, t5] then "well." else "wrong."))
